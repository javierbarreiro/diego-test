import { NgModule } from '@angular/core';
import { RouterModule, Routes, Router } from '@angular/router';

import { AddMenuRoutes } from '@ind-studio/menu';

const routes: Routes = [
    { path: '', redirectTo: '/tools/alarms-online', pathMatch: 'full' },
];

@NgModule({
    imports: [
        RouterModule.forChild([])
    ],
    exports: [
        RouterModule
    ],
    declarations: [],
    entryComponents: []
})
export class AppRoutingModule {
    constructor(private router: Router) {
        AddMenuRoutes(router, routes);
    }
}
